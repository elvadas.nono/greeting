FROM maven:3-jdk-11-openj9  AS build
COPY ./src  /usr/src/app/src
COPY ./pom.xml /usr/src/app/pom.xml
RUN mvn -f /usr/src/app/pom.xml clean package


FROM openjdk:15-jdk-alpine3.11
COPY --from=build /usr/src/app/target/rest-service-0.0.1-SNAPSHOT.jar  /usr/app/rest-service-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/usr/app/rest-service-0.0.1-SNAPSHOT.jar"]